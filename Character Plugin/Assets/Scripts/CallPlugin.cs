﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class CallPlugin : MonoBehaviour
{
    [SerializeField] Material mat;

    Texture2D tex;

    [DllImport("PluginForUnity")]
    private static extern void SetTimeFromUnity(float t);

    [DllImport("PluginForUnity")]

    private static extern void SetTextureFromUnity(System.IntPtr texture);


    IEnumerator Start()
    {
        CreateTextureAndPassToPlugin();
        yield return StartCoroutine(CallPluginAtEndOfFrames());
    }

    private void CreateTextureAndPassToPlugin()
    {
        tex = new Texture2D(64, 64, TextureFormat.ARGB32, false);
        tex.filterMode = FilterMode.Point;
        tex.Apply();

        mat.mainTexture = tex;

        SetTextureFromUnity(tex.GetNativeTexturePtr());

    }

    private IEnumerator CallPluginAtEndOfFrames()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            
            SetTimeFromUnity(Time.timeSinceLevelLoad);

            GL.IssuePluginEvent(1);

        }
    }

    public Texture2D GetMask()
    {
        return tex;
    }
}
