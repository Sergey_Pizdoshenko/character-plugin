﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{

    [SerializeField] float speed = 2f;
    [SerializeField] float distance = 5f;
    [SerializeField] float height = 1f;

    [SerializeField] Transform target;

    Vector2 move;
    

   
    void Update()
    {
        move += new Vector2(1 * speed, 0);
        move.y = height;

        Quaternion rotation = Quaternion.Euler(move.y, move.x, 0);

        Vector3 position = target.position - (rotation * Vector3.forward * distance);

        transform.localRotation = rotation;
        transform.localPosition = position;
    }
}
