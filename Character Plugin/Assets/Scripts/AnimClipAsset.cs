﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "BtnInfo", menuName = "Btn Info")]
public class AnimClipAsset : ScriptableObject
{
    public string triggerName;
    public string btnName;
    public int needEnergy = 10;

    
}
