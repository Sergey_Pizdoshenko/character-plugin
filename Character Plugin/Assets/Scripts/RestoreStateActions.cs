﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreStateActions : StateMachineBehaviour
{
    Ellen ellen;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetFloat("Multiplier", 1);
        ellen = animator.GetComponent<Ellen>();
        ellen.StartCoroutine(FadeAnimSpeed(animator));
        
    }


    IEnumerator FadeAnimSpeed(Animator animator) {
        float velocity = 0f;
        while(animator.GetFloat("Multiplier") >= 0.0001f)
        {
            yield return null;
            float multiplier = Mathf.SmoothDamp(animator.GetFloat("Multiplier"), 0f, ref velocity, 0.05f);
            animator.SetFloat("Multiplier", multiplier);
        }
        ellen.StartRestoreEnergy();
        animator.ResetTrigger("Restore");

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    if(ellen.Energy )
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
