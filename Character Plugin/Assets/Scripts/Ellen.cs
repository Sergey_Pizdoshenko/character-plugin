﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ellen : MonoBehaviour
{
    [SerializeField] ProgressBar energyBar;
    [SerializeField] float energy = 100;
    [SerializeField] ParticleSystem aura;


    public float Energy
    {
        get { return energy; }
    }

    float fullEnergy;

    Animator animator;
    Coroutine restoreEnergy;

    // Start is called before the first frame update
    void Start()
    {
        fullEnergy = energy;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        energyBar.value = energy / fullEnergy;

        if (energy <= 0f && animator.GetCurrentAnimatorStateInfo(0).IsName("EllenIdle"))
        {
            animator.SetTrigger("Restore");
        }
    }

    public void StartRestoreEnergy()
    {
        restoreEnergy = StartCoroutine(RestoreEnergy());
    }

    public void DepriveEnergy(float value)
    {
        energy -= value;
        energy = Mathf.Clamp(energy, 0f, float.PositiveInfinity);
        if (restoreEnergy != null)
            StopCoroutine(restoreEnergy);
    }

    IEnumerator RestoreEnergy()
    {
        while (fullEnergy > energy)
        {
            yield return null;
            energy += 0.25f;
        }
        restoreEnergy = null;
        animator.SetFloat("Multiplier", 1f);
    }

    public ParticleSystem GetAura()
    {
        return aura;
    }
}
