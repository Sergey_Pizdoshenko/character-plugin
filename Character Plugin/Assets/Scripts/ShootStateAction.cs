﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShootStateAction : StateMachineBehaviour
{
    [SerializeField] Material unlitTransparent;

    Ellen ellen;
    Material originMat;
    ParticleSystemRenderer psr;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ellen = animator.GetComponent<Ellen>();
        psr = ellen.GetAura().GetComponent<ParticleSystemRenderer>();
        originMat = psr.material;
        psr.material = unlitTransparent;
        unlitTransparent.SetTexture("_AlphaTex", ellen.GetComponent<CallPlugin>().GetMask());

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        psr.material = originMat;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
