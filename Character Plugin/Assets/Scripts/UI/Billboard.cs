﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{

    void Update()
    {
        Vector3 position = transform.position + Camera.main.transform.rotation * Vector3.forward ;

        Vector3 worldUp = Camera.main.transform.rotation * Vector3.up;

        transform.LookAt(position, worldUp);
        
    }
}
