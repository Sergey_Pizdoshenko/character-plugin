﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupStateBtns : MonoBehaviour
{

    [SerializeField] GameObject btnPrefab;
    [SerializeField] Animator animator;

    [SerializeField] AnimClipAsset[] animations;

    // Start is called before the first frame update
    void Start()
    {
        AnimationClip[] animationClips = animator.runtimeAnimatorController.animationClips;
        int countClips = animations.Length;

        for (int i = 0; i < countClips; i++)
        {
            GameObject btnClone = Instantiate(btnPrefab, transform);
            BtnState btnState = btnClone.GetComponent<BtnState>();
            AnimClipAsset aca = animations[i];
            btnState.SetText(animations[i].btnName + " " + aca.needEnergy.ToString());
            btnState.SetAction(() => {
                Ellen ellen = animator.GetComponent<Ellen>();
                if (ellen.Energy >= 0.1f/*aca.needEnergy/**/)
                {
                    bool condition1 = animator.GetCurrentAnimatorStateInfo(0).IsName("EllenIdle");
                    bool condition2 = animator.GetCurrentAnimatorStateInfo(0).IsName("EllenSpawn") && ellen.Energy >= aca.needEnergy;

                    if (condition1 || condition2)
                    {
                        animator.SetTrigger(aca.triggerName);
                        ellen.DepriveEnergy(aca.needEnergy);
                    }
                }
            });
        }

    }

}
