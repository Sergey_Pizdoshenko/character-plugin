﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ProgressBar : MonoBehaviour
{
    [Range(0f,1f)]
    public float value = 1f;

    [Space(8)]
    [SerializeField] Image bar;
    [SerializeField] Image barHide;

    float sizeX;
    float oldValue;

    Coroutine smoothResize;

    void Start()
    {
        sizeX = bar.rectTransform.rect.width;
        oldValue = value;
        
    }

    void Update()
    {
        if(!Mathf.Approximately(oldValue, value) && smoothResize == null)
        {
            smoothResize = StartCoroutine(SmoothResize());
        }

        UpdateBarView(bar, value);
        UpdateBarView(barHide, oldValue);

    }

    IEnumerator SmoothResize()
    {
        float velocity = 0f;
        while(!Mathf.Approximately(oldValue, value))
        {
            yield return null;
            oldValue = Mathf.SmoothDamp(oldValue, value, ref velocity, 0.3f);
        }
        smoothResize = null;
    }

    void UpdateBarView(Image bar, float value)
    {
        float percent = value / 1f;
        float subtractValue = sizeX * percent;

        bar.rectTransform.sizeDelta = new Vector2(subtractValue - sizeX, 0);
        bar.rectTransform.anchoredPosition = new Vector2((subtractValue - sizeX) / 2, 0);
    }
}
