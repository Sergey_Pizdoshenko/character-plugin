﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class BtnState : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI txtState;
    [SerializeField] Image outline;

    [SerializeField] Color normal;
    [SerializeField] Color select;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetText(string txt)
    {
        txtState.text = txt;
    }

    public void SetAction(UnityEngine.Events.UnityAction act)
    {
        GetComponent<Button>().onClick.AddListener(act);
    }
}
